package api;

import model.vo.Taxi;

import java.text.ParseException;

import model.data_structures.MaxPriorityQueue;
import model.vo.CompaniaServicios;
import model.vo.RangoFechaHora;

/**
 * Basic API for testing the functionality of the TaxiTrip manager
 */
public interface ITaxiTripsManager {

	/**
	 * Method to load the services of a specific taxi 
	 * The services are loaded in both a Stack and a Queue 
	 * @param servicesFile - path to the JSON file with taxi services
	 * @return true if the services are correctly loaded, false if not. 
	 */
	public boolean loadServices(String serviceFile, RangoFechaHora range) throws ParseException;
	
	/**
	 * Ordena los taxis ascendentemente por id y los devuelve en un array
	 * @return Arreglo con taxis ordenados ascendentemente.
	 */
	public Taxi[] ordenarTaxisAscendentemente();
	
	/**
	 * Ordena las companias por servicios en una cola de prioridad (de mayor a menor)
	 * @return Cola de prioridad con las companias por servicios.
	 */
	public MaxPriorityQueue<CompaniaServicios> darCompanias();	
}
