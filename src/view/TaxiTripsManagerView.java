package view;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Scanner;


import controller.Controller;
import model.data_structures.MaxPriorityQueue;
import model.logic.TaxiTripsManager;
import model.vo.CompaniaServicios;
import model.vo.RangoFechaHora;
import model.vo.Service;
import model.vo.Taxi;

/**
 * view del programa
 */
public class TaxiTripsManagerView 
{

	public static void main(String[] args) 
	{
		
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			//imprime menu
			printMenu();

			//opcion req
			int option = sc.nextInt();

			switch(option)
			{
			//1C cargar informacion dada
			case 1:

				//imprime menu cargar
				printMenuCargar();

				//opcion cargar
				int optionCargar = sc.nextInt();

				//directorio json
				String linkJson = "";
				switch (optionCargar)
				{
				//direccion json pequeno
				case 1:

					linkJson = TaxiTripsManager.DIRECCION_SMALL_JSON;
					break;

					//direccion json mediano
				case 2:

					linkJson = TaxiTripsManager.DIRECCION_MEDIUM_JSON;
					break;

					//direccion json grande
				case 3:

					linkJson = TaxiTripsManager.DIRECCION_LARGE_JSON;
					break;
				}

				//fecha inicial
				System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
				String fechaInicialCargar = sc.next();

				//hora inicial
				System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
				String horaInicialCargar = sc.next();

				//fecha final
				System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
				String fechaFinalCargar = sc.next();

				//hora final
				System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
				String horaFinalCargar = sc.next();
				
				RangoFechaHora rangoCargar = new RangoFechaHora(fechaInicialCargar, fechaFinalCargar, horaInicialCargar, horaFinalCargar);
				
				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				//Cargar data
				try {
					Controller.cargarSistema(linkJson, rangoCargar);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					System.out.println("Hubo un problema con la carga del archivo");
				}

				//Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime)/(1000000);

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");

				break;

				//1A	
			case 2:
				
				Taxi[] taxisPrimerReq = Controller.reqFunc1();
				System.out.println("Los taxis ordenados ascendentemente por id son:");
				for(int i = 0; i<taxisPrimerReq.length; i++) {
					Taxi actual = taxisPrimerReq[i];
					System.out.println("Taxi id: " +actual.getTaxiId() + ".\t \n Número de servicios: " + actual.getNumServices());
				}
				break;

			case 3: //2A
				
				MaxPriorityQueue<CompaniaServicios> companiasSegundoReq = Controller.reqFunc2();
				for(int i = 0; i < companiasSegundoReq.size(); i++)
				{
					CompaniaServicios companiaServicios = companiasSegundoReq.delMax();
					System.out.println("Información de servicios" );
					System.out.println("Nombre:" + companiaServicios.getNomCompania() + companiaServicios.getServicios().length);
				}

				break;

			case 4:
				fin=true;
				sc.close();
				break;
			}
		}
	}
	/**
	 * Menu 
	 */
	private static void printMenu() //
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 1----------------------");
		System.out.println("Cargar data (1C):");
		System.out.println("1. Cargar toda la informacion dada una fuente de datos (small,medium, large).");

		System.out.println("\nFunciones:\n");
		System.out.println("2.Obtenga una lista con todos los taxis y su respectivo número de servicios asociados en orden ascendente.");
		System.out.println("3.Obtenga la lista de empresas con su información en el orden de su prioridad (de mayor a menor)");
		System.out.println("4. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");

	}

	private static void printMenuCargar()
	{
		System.out.println("-- Que fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- Type the option number for the task, then press enter: (e.g., 1)");
	}

}
