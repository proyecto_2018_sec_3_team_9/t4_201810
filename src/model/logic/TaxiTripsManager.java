package model.logic;

import java.io.File;


import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import api.ITaxiTripsManager;
import model.data_structures.HeapSort;
import model.data_structures.MaxPriorityQueue;
import model.data_structures.MergeSort;
import model.data_structures.ServiceDateComparator;
import model.data_structures.ServiceTaxiIdComparator;
import model.data_structures.TaxiIdComparator;
import model.vo.CompaniaServicios;
import model.vo.RangoFechaHora;
import model.vo.Service;

import model.vo.Taxi;


public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large";


	private  Taxi[] list2;
	private Service[] list1;


	@Override 
	public boolean loadServices(String direccionJson, RangoFechaHora range) throws ParseException 
	{
		boolean cargo = true;

		System.out.println("Cargar sistema con" + direccionJson);
		File arch = new File(direccionJson);
		try 
		{
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			Date initial = format.parse(range.getFechaInicial() + " " + range.getHoraInicio());
			Date finals = format.parse(range.getFechaFinal() + " " + range.getHoraFinal());

			if(arch.isDirectory()) {

				File[] files = arch.listFiles();
				for(int i = 0; i<files.length; i++) {
					Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
					Service[] service = gson.fromJson(new FileReader(files[i].getAbsolutePath()), Service[].class);

					int j = 0;
					for(Service s : service) {

						Date ini = s.getStartTime();
						Date fin = s.getEndTime();
						if(ini != null && fin != null && ini.compareTo(initial) >= 0  && fin.compareTo(finals) <= 0) {
							j++;
						}
					}
					list1 = new Service[j];
					j=0;
					for(Service s : service) {
						Date ini = s.getStartTime();
						Date fin = s.getEndTime();
						if(ini != null && fin != null && ini.compareTo(initial) >= 0  && fin.compareTo(finals) <= 0) {
							list1[j++] = s;
						}
					}
				}
			}
			else {
				Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create();
				Service[] service = gson.fromJson(new FileReader(direccionJson), Service[].class);

				int j = 0;
				for(Service s : service) {
					Date ini = s.getStartTime();
					Date fin = s.getEndTime();
					if(ini != null && fin != null && ini.compareTo(initial) >= 0  && fin.compareTo(finals) <= 0) {
						j++;
					}
				}
				list1 = new Service[j];
				j= 0;
				for(Service s : service) {
					Date ini = s.getStartTime();
					Date fin = s.getEndTime();
					if(ini != null && fin != null && ini.compareTo(initial) >= 0  && fin.compareTo(finals) <= 0) {
						list1[j++] = s;
					}
				}
			}

			if(list1.length > 0) {

				MergeSort.sort(list1, new ServiceTaxiIdComparator(), true);
				Service actual = list1[0];

				int k = 1;
				String idActual = actual.getTaxiId();
				for(int i = 0; i < list1.length; i++) {
					if(!list1[i].getTaxiId().equals(idActual)) {
						idActual = list1[i].getTaxiId();
						k++;
					}
				}
				list2 = new Taxi[k];
				actual = list1[0];
				idActual = actual.getTaxiId();
				k = 0;
				int cuenta = 0;
				double miles = 0.0;
				double money = 0.0;
				for(int i = 0; i < list1.length; i++) {
					Service toCompare = list1[i];
					String taxiId = toCompare.getTaxiId();
					if(idActual.equals(taxiId)) {
						cuenta++;
						miles += toCompare.getTripMiles();
						money += toCompare.getTripTotal();
						if(i==list1.length-1) {
							Taxi taxi = new Taxi(idActual, cuenta, miles, money);
							list2[k] = taxi;
						}
					}
					else {
						Taxi taxi = new Taxi(idActual, cuenta, miles, money);
						list2[k] = taxi;
						k++;
						idActual = taxiId;
						cuenta = 1;
						miles = toCompare.getTripMiles();
						money = toCompare.getTripTotal();
						if(i==list1.length-1) {
							Taxi taxi1 = new Taxi(idActual, cuenta, miles, money);
							list2[k] = taxi1;
						}
					}
				}
			}
			if(list2 != null) {
				System.out.println("el tamaño de la lista de taxis es:" + list2.length);
			}
			else {
				System.out.println("No hay taxis que cumplan con el rango de fechas dado");
			}
		} 
		catch (ParseException pe) {
			cargo = false;
			throw new ParseException("Hubo un error parseando las fechas del rango", 0);
		}
		catch (Exception e) 
		{
			// TODO: handle exception
			e.printStackTrace();
			cargo = false;
		}
		return cargo;
	}

	public Taxi[] ordenarTaxisAscendentemente() 
	{
		HeapSort.sort(list2, new TaxiIdComparator(), true);
		return list2;
	}

	public MaxPriorityQueue<CompaniaServicios> darCompanias()
	{

		MergeSort.sort(list1, new ServiceDateComparator(), true);

		String[] listaCompanias = new String[list1.length];

		for(int i = 0; i < list1.length ;i++)
		{
			Service actual = list1[i];
			String compania = actual.getCompany();

			if (actual != null && compania != null)
			{
				if (listaCompanias.length == 0)
				{
					listaCompanias[i] = compania;
				}
				else
				{
					Boolean encontro = false;

					for (int j = 0; j < listaCompanias.length; j++)
					{
						String nombre = listaCompanias[j];
						if(compania.equals(nombre))
						{
							encontro = true;
						}
					}

					if(!encontro)
					{
						listaCompanias[i] = compania;
					}
				}
			}

		}

		MaxPriorityQueue Pqueue = new MaxPriorityQueue<>(listaCompanias.length);


		for (int i = 0; i < listaCompanias.length; i++)
		{
			String companias2 = listaCompanias[i];
			String nombre= companias2;
			Service[] services= new Service[listaCompanias.length];
			for (Service servicioActual: list1)
			{
				if(servicioActual.getCompany().equals(null))
				{
					nombre =  "Independent Owner";
					services[i] = servicioActual;;

				}
				else if(servicioActual.getCompany().equals(companias2))
				{
					services[i] = servicioActual;
				}
			}

			CompaniaServicios compañia = new CompaniaServicios();
			compañia.setNomCompania(nombre);
			compañia.setServicios(services);
			Pqueue.insert(compañia);
		}
		return Pqueue;
	}
}