package model.data_structures;

import java.util.Comparator;

/**
 * This class was created in base of the implementation of MergeSort that uses Arrays 
 * found on: https://algs4.cs.princeton.edu/22mergesort/Merge.java.html
 */
public class MergeSort<T>{

	// stably merge a[lo .. mid] with a[mid+1 ..hi] using aux[lo .. hi]
	private static void merge(Comparable[] a, Comparable[] aux, int lo, int mid, int hi, Comparator comparator, boolean direction) {
		// precondition: a[lo .. mid] and a[mid+1 .. hi] are sorted subarrays
		assert isSorted(a, lo, mid, comparator, direction);
		assert isSorted(a, mid+1, hi, comparator, direction);

		// copy to aux[]
		for (int k = lo; k <= hi; k++) {
			aux[k] = a[k]; 
		}

		// merge back to a[]
		int i = lo, j = mid+1;
		for (int k = lo; k <= hi; k++) {
			if      (i > mid)              a[k] = aux[j++];
			else if (j > hi)               a[k] = aux[i++];
			else if (less(aux[j], aux[i], comparator,direction)) a[k] = aux[j++];
			else                           a[k] = aux[i++];
		}

		// postcondition: a[lo .. hi] is sorted
		assert isSorted(a, lo, hi, comparator, direction);
	}

	// mergesort a[lo..hi] using auxiliary array aux[lo..hi]
	private static void sort(Comparable[] a, Comparable[] aux, int lo, int hi, Comparator comparator, boolean direction) {
		if (hi <= lo) return;
		int mid = lo + (hi - lo) / 2;
		sort(a, aux, lo, mid, comparator, direction);
		sort(a, aux, mid + 1, hi, comparator, direction);
		merge(a, aux, lo, mid, hi, comparator, direction);
	}

	/**
	 * Rearranges the array in ascending order, using the natural order.
	 * @param a the array to be sorted
	 * comparator The comparator that will be used to know what atribute to use for comparing.
	 * direction The direction of the sorting. If true, it will be ascending. If false, it will be descending.
	 */
	public static void sort(Comparable[] a, Comparator comparator, boolean direction) {
		Comparable[] aux = new Comparable[a.length];
		sort(a, aux, 0, a.length-1, comparator, direction);
		assert isSorted(a, comparator, direction);
	}


	/***************************************************************************
	 *  Helper sorting function.
	 ***************************************************************************/

	// is v < w ?
	private static boolean less(Comparable v, Comparable w, Comparator comparator, boolean direction) {
		if(direction) {
			return comparator.compare(v, w) <= 0;
		}
		else {
			return comparator.compare(w, v) <= 0;
		}
	}

	/***************************************************************************
	 *  Check if array is sorted - useful for debugging.
	 ***************************************************************************/
	private static boolean isSorted(Comparable[] a, Comparator comparator, boolean direction) {
		return isSorted(a, 0, a.length - 1, comparator, direction);
	}

	private static boolean isSorted(Comparable[] a, int lo, int hi, Comparator comparator, boolean direction) {
		for (int i = lo + 1; i <= hi; i++)
			if (less(a[i], a[i-1], comparator, direction)) return false;
		return true;
	}
}
