package model.data_structures;

import java.util.Comparator;

import model.vo.Taxi;

public class TaxiIdComparator implements Comparator<Taxi> {

	@Override
	public int compare(Taxi o1, Taxi o2) {
		// TODO Auto-generated method stub
		int comparar = o1.getTaxiId().compareTo(o2.getTaxiId());

		if(comparar > 0)
		{
			return 1;
		}
		else if(comparar < 0)
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}

}
