/**
 *  The {@code Heap} class provides a static methods for heapsorting
 *  an array.
 *  <p>
 *  For additional documentation, see <a href="https://algs4.cs.princeton.edu/24pq">Section 2.4</a> of
 *  <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 *  This implementation was edited to use Comparators and order in ascending or descending order.
 *  @author Luis Gómez Amado
 */
package model.data_structures;

import java.util.Comparator;

public class HeapSort {
	// This class should not be instantiated.
    private HeapSort() { }

    /**
     * Rearranges the array in ascending order, using the natural order.
     * @param pq the array to be sorted
     */
    public static void sort(Comparable[] pq, Comparator comparator, boolean ascendant) {
        int n = pq.length;
        for (int k = n/2; k >= 1; k--)
            sink(pq, k, n, comparator, ascendant);
        while (n > 1) {
            exch(pq, 1, n--);
            sink(pq, 1, n, comparator, ascendant);
        }
    }

   /***************************************************************************
    * Helper functions to restore the heap invariant.
    ***************************************************************************/

    private static void sink(Comparable[] pq, int k, int n, Comparator comparator, boolean ascendant) {
        while (2*k <= n) {
            int j = 2*k;
            if (j < n && less(pq, j, j+1, comparator, ascendant)) j++;
            if (!less(pq, k, j, comparator, ascendant)) break;
            exch(pq, k, j);
            k = j;
        }
    }

   /***************************************************************************
    * Helper functions for comparisons and swaps.
    * Indices are "off-by-one" to support 1-based indexing.
    ***************************************************************************/
    private static boolean less(Comparable[] pq, int i, int j, Comparator comparator, boolean ascendant) {
		if(ascendant) {
			return comparator.compare(pq[i-1], pq[j-1]) <= 0;
		}
		else {
			return comparator.compare(pq[j-1], pq[i-1]) <= 0;
		}
    }

    private static void exch(Object[] pq, int i, int j) {
        Object swap = pq[i-1];
        pq[i-1] = pq[j-1];
        pq[j-1] = swap;
    }

}
