package controller;

import java.text.ParseException;



import api.ITaxiTripsManager;
import model.data_structures.MaxPriorityQueue;
import model.logic.TaxiTripsManager;
import model.vo.CompaniaServicios;
import model.vo.RangoFechaHora;
import model.vo.Taxi;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static ITaxiTripsManager manager = new TaxiTripsManager();

	//1C
	public static boolean cargarSistema(String direccionJson, RangoFechaHora rango) throws ParseException
	{
		return manager.loadServices(direccionJson, rango);
	}

	public static Taxi[] reqFunc1() {
		return manager.ordenarTaxisAscendentemente();
	}
	
	public static MaxPriorityQueue<CompaniaServicios> reqFunc2()
	{
		return manager.darCompanias();
	}
}
